import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.lightGreen[300],
          appBar:AppBar(
           backgroundColor: Colors.green,
            title: Text("Dice"),
          ),
        body: DicePage(),
        )
    );
  }
}


class DicePage extends StatefulWidget {
  //const DicePage({Key key}) : super(key: key);

  @override
  _DicePageState createState() => _DicePageState();
}

class _DicePageState extends State<DicePage> {
  int leftDiceNumber = 1;
  int rightDiceNumber = 5;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Row(
          children: [
            Expanded(
              child: GestureDetector(
                onTap: (){
                  setState(() {
                    leftDiceNumber = Random().nextInt(6)+1;
                  });
                },
              child: Padding(
                padding: EdgeInsets.all(16),
                child: Image(
                  image: AssetImage("assets/images/dice${leftDiceNumber.toString()}.png"),
                ),
              ),
              ),
            ),
            Expanded(
              child:GestureDetector(
                onTap: (){
                  setState(() {
                    rightDiceNumber = Random().nextInt(6)+1;
                  });
                },
              child: Padding(
                padding: EdgeInsets.all(16),
                child: Image(
                  image: AssetImage("assets/images/dice${rightDiceNumber.toString()}.png"),
                ),
              ),
            ),
            )

          ]
        )
      ),
    );
  }
}


